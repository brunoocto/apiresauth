import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

require('./bootstrap');

Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue').default
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue').default
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue').default
);

import App from './views/App';
import Home from './views/Home';
import Hello from './views/Hello';
import PassportClients from './views/passport/Clients';
import PassportAuthorizedClients from './views/passport/AuthorizedClients';
import PassportPersonalAccessTokens from './views/passport/PersonalAccessTokens';

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
        },
        {
            path: '/hello',
            name: 'hello',
            component: Hello,
        },
        {
            path: '/passport-clients',
            name: 'passport-clients',
            component: PassportClients,
        },
        {
            path: '/passport-authorized-clients',
            name: 'passport-authorized-clients',
            component: PassportAuthorizedClients,
        },
        {
            path: '/passport-personal-access-tokens',
            name: 'passport-personal-access-tokens',
            component: PassportPersonalAccessTokens,
        },
    ],
});

const app = new Vue({
    el: '#app',
    components: { App },
    router,
});
