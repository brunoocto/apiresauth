<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();

        // Encryption file for security
        Passport::loadKeysFrom(storage_path());

        // Add 100 years to make no expiration time
        Passport::tokensExpireIn(now()->addYears(100));

        // Add 100 years to make no expiration time
        Passport::refreshTokensExpireIn(now()->addYears(100));
    }
}
