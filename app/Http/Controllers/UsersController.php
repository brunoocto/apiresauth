<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function user()
    {
        // Get the Authenticated user via Bearer token
        $user = \Auth::user();
        // Check if the user exists
        if ($user) {
            // Return the user information
            return response()->json([
                'success' => true,
                'user' => $user,
            ]);
        } else {
            // Failed to authenticate the user with the token sent
            return response()->json([
                'success' => false,
                'message' => 'Invalid Token',
            ], 401);
        }
    }

    public function logout(Request $request)
    {
        $token = $request->user()->token();
        if ($token) {
            $token->revoke();
            $response = ['message' => 'You have been successfully logged out!'];
            return response($response, 204);
        }
        $response = ['message' => 'You are already logged out.'];
        return response($response, 400);
    }
}
