<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Routes based on this tutorial
 * https://medium.com/@godilite/laravel-6-api-authentication-with-laravel-passport-72dccc5c47f7
 */

Route::group([], function () {
    Route::get('/user', 'UsersController@user')->middleware('auth:api');
    Route::post('/user/logout', 'UsersController@logout')->middleware('auth:api');
});
